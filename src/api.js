import axios from 'axios'
import {FACEID_RECOGNIZE, FACEID_REGISTER, OCR_FRONTBACK, EKYC_MATCH, HEADPOSE} from  '../apiKeyEnv'
export const request = () => {
    let baseURL =  "https://endpointh.eyeq.tech/api"  // "http://192.168.1.33:25000"
    let ekycRNAppAxios = axios.create({
        headers: {
            "Accept": "application/json",
            // "Content-Type": "application/json",
        }
    })
    let faceIDAxios = axios.create({
        headers: {
            "Authorization": FACEID_RECOGNIZE,
            "Accept": "application/json",
            "Content-Type": "multipart/form-data",
            "processData": false
        }
    })
    let faceIDRegisterAxios = axios.create({
        headers: {
            "Authorization": FACEID_REGISTER,
            "Accept": "application/json",
            "Content-Type": "multipart/form-data",
            "processData": false
        }
    })
    let ocrAxios = axios.create({
        headers: {
            "Authorization": OCR_FRONTBACK,
            "Accept": "application/json",
            "Content-Type": "multipart/form-data",
            "processData": false
        }
    })
    let matchAxios = axios.create({
        headers: {
            "Authorization": EKYC_MATCH,
            "Accept": "application/json",
            "Content-Type": "multipart/form-data",
            "processData": false
        }
    })
    let headposeAxios = axios.create({
        headers: {
            "Authorization": HEADPOSE,
            "Accept": "application/json",
            "Content-Type": "multipart/form-data",
            "processData": false
        }
    })
    return {
        getInfo: function(aliasId, data) {
            return ekycRNAppAxios.get(baseURL + "/ekycRNApp?aliasId=" + aliasId, {aliasId, data})
        },
        registerToDB: function(aliasId, data, finger) {
            return ekycRNAppAxios.post(baseURL + "/ekycRNApp", {aliasId, data, finger})
        },
        registerFinger: function(uri) {
            let formdata = new FormData();
            formdata.append("file", {uri: uri, name: 'xxx.jpg', type: 'image/jpg'})
            return ekycRNAppAxios.post(baseURL+"/ekycRNApp/enhanced", formdata)
        },
        verifyFinger: function(uri, aliasId) {
            let formdata = new FormData();
            formdata.append("file", {uri: uri, name: 'xxx.jpg', type: 'image/jpg'})
            formdata.append("aliasId", aliasId)
            return ekycRNAppAxios.post(baseURL+"/ekycRNApp/verifyFinger", formdata)
        },
        headpose: function(uri, seq) {
            let formdata = new FormData();
            formdata.append("file", {uri: uri, name: 'xxx.mp4', type: 'video/mp4'})
            formdata.append("sequence", seq)
            return headposeAxios.post(baseURL + "/headpose/verify", formdata)
        },
        recognize: function(uri) {
            let formdata = new FormData();
            formdata.append("video", {uri: uri, name: 'xxx.mp4', type: 'video/mp4'})
            return faceIDAxios.post(baseURL + "/faceid/recognize", formdata)
        },
        register: function(uri, aliasId) {
            let formdata = new FormData();
            formdata.append("video", {uri: uri, name: 'xxx.mp4', type: 'video/mp4'})
            formdata.append("aliasId", aliasId)
            return faceIDRegisterAxios.post(baseURL + "/faceid/register", formdata)
        },
        ocr: function(front, back) {
            let formdata = new FormData();
            formdata.append("front", {uri: front, name: 'front.jpg', type: 'image/jpg'})
            formdata.append("back", {uri: back, name: 'back.jpg', type: 'image/jpg'})
            return ocrAxios.post(baseURL + "/ocr/front_back", formdata)
        },
        match: function(face, front) {
            let formdata = new FormData();
            formdata.append("image1", {uri: face, name: 'face.jpg', type: 'image/jpg'})
            formdata.append("image2", {uri: front, name: 'front.jpg', type: 'image/jpg'})
            return matchAxios.post(baseURL + "/ekyc/match", formdata)
        }
    }
}
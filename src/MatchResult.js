import {
    Button,
    Icon,
    ButtonGroup,
    Spinner,
    Card,
    ListItem,
    Layout,
  } from '@ui-kitten/components';
import { StyleSheet, View, Text, Image } from 'react-native';
import React from 'react';
const LoadingIndicator = (props) => (
    <View style={[props.style, styles.indicator]}>
      <Spinner status="basic" size='small'/>
    </View>
);
class MatchResult extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            uri_front: null,
            uri_back: null,
            requesting: false,
            ocr_data: null
        }
    }
    render() {
        console.log(this.state.uri_front)
        return (
            <Layout style={styles.container}>
                <Layout style={{flex: 1, flexDirection:'column', alignItems:'center', overflow:'hidden'}}>
                    <Image source={this.state.uri_front ? {uri: this.state.uri_front} : require('../assets/placeholder.jpg')} style={{flex: 1, marginBottom: 16, width:"100%"}}/>
                    <Image source={this.state.uri_back ? {uri: this.state.uri_back} : require('../assets/placeholder.jpg')}  style={{flex: 1, marginBottom: 16, width:"100%"}}/>
                </Layout>
                <Layout style={{flexDirection: 'row', justifyContent:'center', marginBottom: 16}}>
                    <ButtonGroup  size='small'> 
                        <Button disabled={this.state.requesting} onPress={this.handleNavigateFront} accessoryLeft={style=><Icon {...style} name="person-outline"/>}>Capture front side</Button>
                        <Button disabled={this.state.requesting} onPress={this.handleNavigateBack}  accessoryLeft={style=><Icon {...style} name="credit-card-outline"/>}>Capture back side</Button>
                    </ButtonGroup>
                </Layout>
                <Button accessoryLeft={this.state.requesting ? LoadingIndicator : style=><Icon {...style} name="cloud-upload-outline"/>} disabled={this.state.requesting || !this.state.uri_back || !this.state.uri_front} onPress={this.getOCR}>Submit</Button>
            </Layout>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        padding: 16,
    }
});
export default MatchResult
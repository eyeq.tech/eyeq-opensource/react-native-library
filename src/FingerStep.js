import {
    Button,
    Icon,
    ButtonGroup,
    Spinner,
    Card,
    Modal,
    Layout,
  } from '@ui-kitten/components';
import { StyleSheet, View, Text, Image } from 'react-native';
import React from 'react';
import {request} from './api'
const LoadingIndicator = (props) => (
    <View style={[props.style, styles.indicator]}>
      <Spinner status="basic" size='small'/>
    </View>
);
class FingerStep extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            uri_finger: null,
            showModal: false
        }
    }
    setUri = (uri) => this.setState({uri_finger: uri})
    handleNavigate = () => {
        this.props.navigation.navigate("CameraFinger", {origin: "FingerStep", callback: this.setUri})
    }
    nextStep = () => {
        this.setState({requesting: true})
        if (this.props.route.params.task === "register") {
            request().registerFinger(this.state.uri_finger)
            .then(res => {
                if (res.data.success) {
                    console.log(res.data)
                    this.props.navigation.navigate("OcrStep",
                    {
                        origin: "OcrStep",
                        faceVideo: this.props.route.params.faceVideo,
                        faceImage: this.props.route.params.faceImage,
                        fingerImage: res.data.file
                    })
                }
                this.setState({requesting: false})
            })
        } else if (this.props.route.params.task === "login") {
            request().verifyFinger(this.state.uri_finger, this.props.route.params.aliasId)
            .then(res => {
                if (res.data.success) {
                    console.log(res.data)
                    this.props.navigation.navigate("Welcome", {aliasId: this.props.route.params.aliasId})
                } else {
                    this.setState({requesting: false, showModal: true})
                    
                }
                
            })
        }
       
        
    }
    tryAgain = () => {
        this.props.navigation.navigate("FaceRecord")
    }
    render() {
        return (
            <Layout style={styles.container}>
                <Modal
                    visible={this.state.showModal}
                    backdropStyle={styles.backdrop}
                    onBackdropPress={() => this.setState({code: undefined})}>
                    <Card disabled={true}>
                    <Text>Code: {this.state.code}</Text>
                    <Button accessoryLeft={x => <Icon {...x} name="refresh-outline"/>} onPress={() => this.tryAgain()}>
                        Try again
                    </Button>
                    </Card>
                </Modal>
                <Layout style={{flex: 1, flexDirection:'row', alignItems:'center', overflow:'hidden'}}>
                    <Image source={this.state.uri_finger ? {uri: this.state.uri_finger} : require('../assets/placeholder.jpg')} style={{flex: 1, marginBottom: 16, width:"100%"}}/>
                </Layout>
                <Layout style={{flexDirection: 'row', justifyContent:'center', marginBottom: 16}}>
                        <Button disabled={this.state.requesting} onPress={this.handleNavigate} accessoryLeft={style=><Icon {...style} name="person-outline"/>}>Capture finger</Button>
                </Layout>
                <Button accessoryLeft={this.state.requesting ? LoadingIndicator : style=><Icon {...style} name="cloud-upload-outline"/>} disabled={this.state.requesting || !this.state.uri_finger} onPress={this.nextStep}>Submit</Button>
            </Layout>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        padding: 16,
    },
    buttonGroup: {
      margin: 8
    },
    btn: {
        margin: 4
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
});
export default FingerStep
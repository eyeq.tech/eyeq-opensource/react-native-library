import {
    Button,
    Icon,
    ButtonGroup,
    Spinner,
    Card,
    ListItem,
    Layout,
  } from '@ui-kitten/components';
import { StyleSheet, View, Text, Image } from 'react-native';
import React from 'react';
const LoadingIndicator = (props) => (
    <View style={[props.style, styles.indicator]}>
      <Spinner status="basic" size='small'/>
    </View>
);
function Success(props) {
    return (
        <Layout style={styles.container}>
            <Text style={{color: "#2194EA", fontSize: 32}}>Register successful</Text>
            <Image source={require('../assets/success.png')} style={{flex: 1, width:"100%"}} />
            <Button onPress={()=>props.navigation.navigate("FaceRecord")} accessoryLeft={x => <Icon name="home-outline" {...x} />}>Home</Button>
        </Layout>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        flexDirection: 'column',
        padding: 16,
    }
});
export default Success
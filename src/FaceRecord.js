import {
    Button,ButtonGroup,
    Layout ,
    Modal,
    Card,
    Spinner,
    Icon,
    Text,
    Input
  } from '@ui-kitten/components';
import { StyleSheet, View, Image, ActivityIndicator } from 'react-native';
import Camera from './Component/Camera'
import {request} from './api'
import axios from 'axios'
import React from 'react';
const LoadingIndicator = (props) => (
    <View style={[props.style, styles.indicator]}>
      <Spinner status="basic" size='small'/>
    </View>
);
const StarIcon = (props) => (
    <Icon {...props} name='star'/>
);
const AlertIcon = (props) => (
    <Icon {...props} name='alert-circle-outline'/>
);
class FaceRecord extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            requestingRecognize: false,
            requestingHeadpose: false,
            requestingRegister: false,
            input_aliasId: "",
            code: undefined,
            headposeStatus: 0
        }
        this.uri = null
        this.uriImage = null
        this.uriHeadpose = null
        this.seq = null
    }
    setUri = (uri) => {
        this.uri = uri
    }
    setUriImage = (uri) => {
        this.uriImage = uri
    }
    setHeadposeUri = (uri, seq) => {
        this.uriHeadpose = uri
        this.seq = seq
        this.callHeadposeApi(uri, seq)
    }
    callHeadposeApi = (uri, seq) => {
        this.setState({requestingHeadpose: true})
        console.log("Headpose video: " + uri)
        console.log("Sequence: " + seq)
        request().headpose(uri, seq)
        .then(res => {
            console.log(res.data)
            if (res.data.result=="OK") { // SKIP
                this.setState({requestingHeadpose: false, headposeStatus: 1})
            } else {
                this.setState({requestingHeadpose: false, headposeStatus: -1})
            }
            this.callVerifyApi(this.uri)
        })
    }
    callVerifyApi = (uri) => {
        this.setState({requestingRecognize: true})
        console.log("FaceRecord: " + uri)
        request().recognize(uri)
        .then(res => {
            console.log(res.data)
            if (res.data.status === "successful") {
                this.setState({requestingRecognize: false, headposeStatus: 0})
                this.props.navigation.navigate("FingerStep", {task: "login", aliasId: res.data.data.aliasId})
                // this.props.navigation.navigate("Welcome", {aliasId: res.data.data.aliasId})
                return
            }
            this.setState({code: res.data.code, requestingRecognize: false})
            console.log("Code " + res.data.code)})
        .catch(err => console.log('!! ' + err))
    }
    callRegisterApi = () => {
        this.setState({requestingRegister: true})
        setTimeout(function() {
            this.setState({requestingRegister: false, code: undefined, headposeStatus: 0})
            this.props.navigation.navigate("FingerStep", {faceVideo: this.uri, faceImage: this.uriImage, task: "register"})
        }.bind(this), 200)
    }
    handleNavigateVideo = () => {
        this.setState({headposeStatus: 0})
        this.props.navigation.navigate("HeadposeCamera", {origin: "FaceRecord", video: true, callback: this.setUri, callbackImage: this.setUriImage, callbackHeadpose: this.setHeadposeUri})
    }
    render() {
        
        return (
            <Layout style={styles.container}>
                {this.state.headposeStatus !== 0 &&
                <Layout style={{marginBottom: 8, flexDirection: 'row', justifyContent:'center', alignItems:"center",padding: 4, borderColor: this.state.headposeStatus == 1 ? "#2194EA" : "#ea2121", borderWidth:1, backgroundColor: this.state.headposeStatus == 1 ? "rgba(33, 148, 234, .5)" : "rgba(234, 33, 33, .5)"}}>
                    <Text style={{color: this.state.headposeStatus == 1 ? '#2194EA' : "#ea2121"}}>
                        {this.state.headposeStatus == 1 ? "Pass antispoofing" : "Failed"}
                    </Text>
                </Layout>
                }
                {this.state.requestingHeadpose &&
                <Layout style={{marginBottom: 8, flexDirection: 'row', justifyContent:'center', alignItems:"center",padding: 4, borderColor:"#88c9db", borderWidth:1, backgroundColor:"rgba(136, 201, 219, .5)"}}>
                    <Spinner size="tiny" />
                    <Text style={{color:"#28748a"}}>   Checking spoofing</Text> 
                </Layout>}
                {this.state.requestingRecognize &&
                <Layout style={{marginBottom: 8, flexDirection: 'row', justifyContent:'center', alignItems:"center",padding: 4, borderColor:"#88c9db", borderWidth:1, backgroundColor:"rgba(136, 201, 219, .5)"}}>
                    <Spinner size="tiny" />
                    <Text style={{color:"#28748a"}}>   Recognizing</Text> 
                </Layout>}
                
                <Image source={require('../assets/male-placeholder-image.jpeg')} style={{flex: 1, width: '100%', marginBottom: 8}} />
                <Button accessoryLeft={this.state.requestingHeadpose || this.state.requestingRecognize ? LoadingIndicator : style => <Icon name="video-outline" {...style} />}   disabled={this.state.requestingHeadpose || this.state.requestingRecognize} onPress={this.handleNavigateVideo}>Record face</Button>
                <Modal
                    visible={this.state.code!==undefined && this.state.code !== "0003"}
                    backdropStyle={styles.backdrop}
                    onBackdropPress={() => this.setState({code: undefined})}>
                    <Card disabled={true}>
                    <Text>Code: {this.state.code}</Text>
                    <Button accessoryLeft={x => <Icon {...x} name="refresh-outline"/>} onPress={() => this.setState({code: undefined})}>
                        Try again
                    </Button>
                    </Card>
                </Modal>
                <Modal
                    visible={this.state.code == "0003"}
                    backdropStyle={styles.backdrop}
                    onBackdropPress={() => {}}>
                    <Card disabled={true}>
                        <Layout style={{flexDirection:'column', marginBottom: 16}}>
                            <Text>You haven't registered in our system.</Text><Text>Let's do it 🌟</Text>
                        </Layout>
                    <ButtonGroup>
                        <Button disabled={this.state.requestingRegister} onPress={this.callRegisterApi} accessoryLeft={this.state.requestingRegister ? LoadingIndicator : StarIcon}>
                            Register
                        </Button>
                        <Button disabled={this.state.requestingRegister} onPress={()=>{this.setState({uri: null, code: undefined})}} accessoryLeft={style=><Icon {...style} name="slash-outline"/>}>
                            Cancel
                        </Button>
                    </ButtonGroup>
                    </Card>
                </Modal>
            </Layout >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        padding: 16,
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
  });
export default FaceRecord
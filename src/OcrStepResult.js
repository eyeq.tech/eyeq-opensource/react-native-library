import {
    Button,
    Input,
    Datepicker,
    Icon,
    Spinner,
    Card,
    Modal,
    Text,
    Layout
  } from '@ui-kitten/components';
import { StyleSheet, View, Platform, Image } from 'react-native';
import React from 'react';
import {request} from './api'
const CalendarIcon = (props) => (
    <Icon {...props} name='calendar'/>
  );
const LoadingIndicator = (props) => (
    <View style={[props.style, styles.indicator]}>
      <Spinner status="basic" size='small'/>
    </View>
);
const randomString = (length) => {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }
const f = (st) => {
    let stt= st.slice(0, 2) + "/" + st.slice(3, 5) + "/" + st.slice(6)
    console.log(stt)
    return stt
}
class OcrStepResult extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            requesting: false,
            name: props.route.params.name,
            id: props.route.params.id,
            birthday: new Date("03/11/1998"),
            address: props.route.params.address,
            ekycRequesting: false,
            ocrRequesting: false,
            match: false,
            error: false
        }
    }
    componentDidMount() {
        this.setState({ekycRequesting: true})
        const {faceImage, cardFront, cardBack} = this.props.route.params
        console.log("Face: " + faceImage)
        console.log("CardFront: " + cardFront)
        request().match(faceImage, cardFront)
        .then(res => {
            if (res.data.status === "match") {
                this.setState({ekycRequesting: false, match: true, ocrRequesting: true})
                request().ocr(cardFront, cardBack)
                .then(res => {
                    console.log(res.data)
                    if (res.data.code != "0000") {
                        this.setState({error: true})
                    }
                    const {id, name, birthday, address} = res.data.data
                    this.setState({id, name, birthday: new Date(f(birthday)), address, ocrRequesting: false})
                })
            } else {
                this.setState({ekycRequesting: false, match: false})
            }
        })
    }
    save = () => {
        this.setState({requesting: true})
        const {faceVideo} = this.props.route.params
        let name = this.state.name.split(" ")
        name = name[name.length - 1]
        let year = this.state.birthday.getFullYear()
        let aliasID = name + year + "_" + randomString(5)
        aliasID = aliasID.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
        console.log(aliasID)
        request().register(faceVideo, aliasID)
        .then(res => {
            console.log(res.data)
            if (res.data.status == "successful") {
                request().registerToDB(aliasID, {name: this.state.name, id: this.state.id, address: this.state.address, birthday: this.state.birthday}, this.props.route.params.fingerImage)
                .then(res => {
                    if (res.data.success) {
                        this.props.navigation.navigate("Success")
                        this.setState({requesting: false})
                    }
                    else {
                        this.setState({requesting: false}) 
                    }
                })
                
            }
        })
    }
    render() {
        return (
            <Layout style={styles.container}>
                <Modal
                    visible={this.state.error}
                    backdropStyle={styles.backdrop}
                    onBackdropPress={() => this.setState({error: false})}>
                    <Card disabled={true}>
                    <Text>Code: </Text>
                    <Button accessoryLeft={x => <Icon {...x} name="refresh-outline"/>} onPress={() => this.props.navigation.navigate("OcrStep")}>
                        Try again
                    </Button>
                    </Card>
                </Modal>
                <Card>
                    <Text>Does your face match with your ID ?</Text>
                    {this.state.ekycRequesting ? <Spinner/> : <Layout>{this.state.match ? <Button style={{marginTop: 16}} accessoryLeft={x => <Icon {...x} name="done-all-outline"/>} appearance='outline'>Matched</Button> : <Button status="danger" accessoryLeft={x => <Icon {...x} name="close-outline" />} style={{marginTop: 16}} appearance='outline'>Unmatched</Button>}</Layout>}
                </Card>
                {
                    !this.state.ekycRequesting && !this.state.match &&
                    <Button onPress={this.save} style={{marginTop: 16}} accessoryLeft={style=><Icon {...style} name="home-outline"/>} >Home</Button>
                }
                {
                !this.state.ekycRequesting && this.state.match &&
                <>
                <Card style={{marginTop: 8}}>
                    <Text>OCR Result</Text>
                    {
                        !this.state.ocrRequesting
                        ?
                        <>
                            <Input
                                label="Name"
                                placeholder='Name'
                                value={this.state.name}
                                onChangeText={nextValue => this.setState({name: nextValue})}
                            />  
                            <Input
                                label="ID Number"
                                placeholder='Id number'
                                value={this.state.id}
                                onChangeText={nextValue => this.setState({id: nextValue})}
                            />  
                            <Datepicker
                                label="Birthday"
                                accessoryRight={CalendarIcon}
                                date={new Date(this.state.birthday)}
                                min={new Date("1/1/1970")}
                                onSelect={nextDate => this.setState({birthday: nextDate})}
                            /> 
                            <Input
                                label="Address"
                                placeholder='Address'
                                value={this.state.address}
                                onChangeText={nextValue => this.setState({address: nextValue})}
                            /> 
                        </>
                        :
                        <Spinner/>
                    }
                </Card>
                <Button disabled={this.state.requesting || this.state.ocrRequesting} onPress={this.save} style={{marginTop: 16}} accessoryLeft={this.state.requesting ? LoadingIndicator : style=><Icon {...style} name="checkmark"/>} >Save</Button>
                </>
                }
                
            </Layout>
            
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        padding: 16,
    },backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
});
export default OcrStepResult
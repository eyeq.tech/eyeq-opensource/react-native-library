import {
    Button,
    Icon,
    ButtonGroup,
    Layout
  } from '@ui-kitten/components';

import { StyleSheet, View, Platform, Image } from 'react-native';
import React from 'react';

class EditPhoto extends React.Component {
    navi = () => {
        const {callback, uri} = this.props.route.params
        callback(uri)
        this.props.navigation.navigate("OcrStep", {origin: "EditPhoto"})
    }
    render() {
        const {uri} = this.props.route.params
        return (
            <Layout style={styles.container}>
                <View style={{flexDirection: 'row', alignItems:'center', justifyContent:'center'}}>
                    <Button style={styles.btn}  status='basic' onPress={()=>this.setState({uri: null})}  size='small'>Redo</Button>
                    <ButtonGroup style={styles.buttonGroup} appearance='outline' size='small'>
                        <Button accessoryLeft={style=><Icon name="flip-outline" {...style}/>} />
                        <Button accessoryLeft={style=><Icon name="flip-2-outline" {...style}/>}/>
                        <Button accessoryLeft={style=><Icon name="refresh-outline" {...style}/>}/>
                    </ButtonGroup>
                    <Button 
                        style={styles.btn} 
                        disabled={false} 
                        size='small'
                        onPress={this.navi}    
                    >
                        Done
                    </Button>
                
                </View>
                <Layout style={{flex :1}}>
                    <Image style={styles.image} source={{uri: uri}} />
                </Layout>
            </Layout>
        )
    }
}
const styles = StyleSheet.create({
    image: {
        flex: 1
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        padding: 16,
    },
    buttonGroup: {
      margin: 8
    },
    btn: {
        margin: 4
    }
  });
export default EditPhoto
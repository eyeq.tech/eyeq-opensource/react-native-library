import {
    Button,
    Icon,
    Layout
  } from '@ui-kitten/components';
import { StyleSheet, View, Platform, Image, Dimensions  } from 'react-native';
import React from 'react';
import {RNCamera} from 'react-native-camera'
import ImageEditor from "@react-native-community/image-editor";

class CameraFinger extends React.Component {
    constructor(props) {
        super(props)
        // this.props.navigation.getParam('origin', null)
        this.state = {
            recording: false,
            disable: false,
            facingfront: false,
            x: 0, y: 0, w:0, h:0
        }
    }
    getIcon() {
        if (!this.state.recording) {
            return this.props.route.params.video ? 'video-outline' : 'camera-outline'
        }
        return 'pause-circle-outline'
    }
    capture = async () => {
        console.log(this.camera.getAvailablePictureSizes())
        this.setState({disable: true})
        const res = await this.camera.takePictureAsync({quality: 1})
        console.log("result from camera: " + res.uri)
        
        if (!this.props.video) {
            console.log(this.state)
            let imageWidth = res.height
            let imageHeight = res.width
            let deviceWidth = Dimensions.get('window').width
            let deviceHeight = Dimensions.get('window').height
            console.log("capture", imageWidth, imageHeight, res.pictureOrientation)
            console.log("screen", deviceWidth, deviceHeight)
            let offset = {y: this.state.x / deviceWidth * imageWidth, x: this.state.y/deviceHeight * imageHeight}
            let size = {height: this.state.w / deviceWidth * imageWidth, width: (this.state.w / deviceWidth * imageWidth) *2/3}
            console.log(size)
            ImageEditor.cropImage(res.uri, {
                offset: offset,
                size: size
            }).then(url => {
                console.log(url)
                this.props.navigation.navigate('EditPhotoFinger', {uri: url, callback: this.props.route.params.callback})
            })
            
        }
        this.setState({disable: false})
    }
    record = async () => {
        console.log(this.camera.getAvailablePictureSizes())
        this.setState({disable: true})
        const res = await this.camera.recordAsync({quality: 1, maxDuration: 1})
        const res2 = await this.camera.takePictureAsync({quality: 1})
        this.setState({disable: false})
        this.props.route.params.callback(res.uri)
        this.props.route.params.callbackImage(res2.uri)
        this.props.navigation.navigate('FaceRecord')
    }
    render() {
        
        return (
            <Layout style={{flex: 1, backgroundColor: 'transparent',position: 'relative'}}>
                
                <View style={{flex: 1, width:'100%', height:"100%"}}>
                    <RNCamera 
                        // width={768}
                        // height={1024}
                        autoFocus={RNCamera.Constants.AutoFocus.on}
                        ref={ref => { this.camera = ref; }}
                        captureAudio={false}
                        type={this.state.facingfront ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back}
                        flashMode={RNCamera.Constants.FlashMode.torch}
                        style={{flex: 1, width:'100%', height:"100%"}}
                        androidCameraPermissionOptions={{
                            title: 'Permission to use camera',
                            message: 'We need your permission to use your camera',
                            buttonPositive: 'Ok',
                            buttonNegative: 'Cancel',
                        }}
                    />
                </View>
                
                
                <Layout style={{position:"absolute", width:"100%", height:"100%", alignItems:"center", justifyContent:"center",backgroundColor:"transparent"}}>
                    <Layout onLayout={e => {
                        this.setState({
                            x: e.nativeEvent.layout.x,
                            y: e.nativeEvent.layout.y,
                            w: e.nativeEvent.layout.width,
                            h: e.nativeEvent.layout.height 
                        })
                    }} style={{backgroundColor:"transparent", flex:1, width:300, height:200, position:"absolute", borderColor:"#369be9", borderWidth: 4}}>
                    </Layout>
                </Layout>
                
                <View style={{flex:1, flexDirection: 'row' ,position: 'absolute', left: 0, right: 0, bottom: 10, justifyContent: 'center', alignItems: 'center'}}>
                    <Button disabled={this.state.disable} onPress={this.capture} style={styles.btn} appearance='outline' accessoryLeft={style=><Icon {...style} width={24} height={24} name='camera-outline' />}/>
                    <Button disabled={this.state.disable} onPress={()=>this.setState({facingfront: !this.state.facingfront})} style={styles.btn} appearance='outline' accessoryLeft={style=><Icon {...style} width={24} height={24} name='flip-outline' />}/>
                </View>
            </Layout>
        )
    }
}

const styles = StyleSheet.create({
    btn: {
    width: 75,
    height: 75,
    borderRadius: 50,
    margin:24
    }
  });
export default CameraFinger
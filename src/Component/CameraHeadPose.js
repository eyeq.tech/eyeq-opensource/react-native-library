import {
    Button,
    Icon,
    Layout,
    Text,
    Card
  } from '@ui-kitten/components';
import { StyleSheet, View, Platform, Image } from 'react-native';
import React from 'react';
import {RNCamera} from 'react-native-camera'
const getDir = ch => {
    if (ch == "L") return "to the LEFT"
    if (ch == "R") return "to the RIGHT"
    if (ch == "U") return "UP"
    if (ch == "D") return "DOWN"
}
class CameraHeadPose extends React.Component {
    constructor(props) {
        super(props)
        // this.props.navigation.getParam('origin', null)
        this.state = {
            recording: false,
            disable: false,
            facingfront: true,
            count: 0,
            seq: ""
        }
    }
    record = async () => {
        this.setState({disable: true})
        const res = await this.camera.recordAsync({quality: 1, maxDuration: 4})
        const res2 = await this.camera.takePictureAsync({quality: 1})
        this.setState({seq: "LRUD", count: 1})
        this.interval = setInterval(function() {
            if (this.state.count == 5) {
                clearInterval(this.interval)
                this.setState({count: 0})
                return
            }
            this.setState({count: this.state.count+1})
        }.bind(this), 2300)  
        
        const res3 = await this.camera.recordAsync({quality: 1, maxDuration: 10})
        this.setState({disable: false})
        this.props.route.params.callback(res.uri)
        this.props.route.params.callbackImage(res2.uri)
        this.props.route.params.callbackHeadpose(res3.uri, "LRUD")
        this.props.navigation.navigate('FaceRecord')
    }
    render() {
        
        return (
            <Layout style={{flex: 1, backgroundColor: 'transperant'}}>
                
                <View style={{flex: 1, width:'100%', height:"100%"}}>
                    <RNCamera 
                        ref={ref => { this.camera = ref; }}
                        captureAudio={false}
                        type={this.state.facingfront ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back}
                        flashMode={RNCamera.Constants.FlashMode.off}
                        style={{flex: 1, width:'100%', height:"100%"}}
                        androidCameraPermissionOptions={{
                            title: 'Permission to use camera',
                            message: 'We need your permission to use your camera',
                            buttonPositive: 'Ok',
                            buttonNegative: 'Cancel',
                        }}
                    />
                </View>
                
                <Layout style={{position:'absolute', top: 10, width: "100%",backgroundColor: 'rgba(255,255,255,.5)', padding: 12, alignItems:"center"}}>
                    <Text>
                        Align your face in the blue area.
                    </Text>
                </Layout>
                <Layout style={{position:"absolute", width:"100%", height:"100%", alignItems:"center", justifyContent:"center",backgroundColor:"transparent"}}>
                    <Layout style={{backgroundColor:"transparent", flex:1, width:200, height:250, position:"absolute", borderTopEndRadius: 50, borderTopStartRadius: 50, borderBottomEndRadius: 100, borderBottomStartRadius: 100,borderColor:"#369be9", borderWidth: 4}}>
                    </Layout>
                </Layout>
                {this.state.count >= 1 && this.state.count <=4 &&
                <Layout style={{position:"absolute", width:"100%", height:"100%", alignItems:"center", justifyContent:"center",backgroundColor:"transparent"}}>
                    <Layout style={{backgroundColor:"transparent", flex:1, width:300, position:"absolute", justifyContent:"center", alignItems:"center"}}>
                    <Text category="h2" style={{color: "white",flexWrap: 'wrap',textAlign: 'center'}}>Turn your head <Text category="h2" style={{fontWeight:'bold'}}>{getDir(this.state.seq[this.state.count - 1])}</Text>,</Text>
                    <Text category="h2" style={{color: "white",flexWrap: 'wrap',textAlign: 'center'}}>then return to straight position.</Text>
                    </Layout>
                    
                </Layout>
                }
                <View style={{flex:1, flexDirection: 'row' ,position: 'absolute', left: 0, right: 0, bottom: 10, justifyContent: 'center', alignItems: 'center'}}>
                    <Button disabled={this.state.disable} onPress={this.record} style={styles.btn} appearance='outline' accessoryLeft={style=><Icon {...style} width={24} height={24} name='video-outline' />}/>
                    <Button disabled={this.state.disable} onPress={()=>this.setState({facingfront: !this.state.facingfront})} style={styles.btn} appearance='outline' accessoryLeft={style=><Icon {...style} width={24} height={24} name='flip-outline' />}/>
                </View>
            </Layout>
        )
    }
}

const styles = StyleSheet.create({
    btn: {
    width: 75,
    height: 75,
    borderRadius: 50,
    margin:24
    }
  });
export default CameraHeadPose
import {
    Button,
    Icon,
    ButtonGroup,
    Spinner,
    Card,
    ListItem,
    Layout,
  } from '@ui-kitten/components';
import { StyleSheet, View, Text, Image } from 'react-native';
import React from 'react';
const LoadingIndicator = (props) => (
    <View style={[props.style, styles.indicator]}>
      <Spinner status="basic" size='small'/>
    </View>
);
class OcrStep extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            uri_front: null,
            uri_back: null
        }
    }
    setUriFront = (uri) => {
        this.setState({uri_front: uri})
    }
    setUriBack = (uri) => {
        this.setState({uri_back: uri})
    }
    capture = async () => {
        const res = await this.camera.takePictureAsync({quality: 1})
        console.log(res)
        this.setState({uri: res.uri})
        console.log(res.uri.split(',')[1])
    }
    handleNavigateFront = () => {
        this.props.navigation.navigate("Camera", {origin: "OcrStep", callback: this.setUriFront})
    }
    handleNavigateBack = () => {
        this.props.navigation.navigate("Camera", {origin: "OcrStep", callback: this.setUriBack})
    }
    getOCR = () => {
        this.props.navigation.navigate("OcrStepResult",
        {
            origin: "OcrStep",
            faceVideo: this.props.route.params.faceVideo,
            faceImage: this.props.route.params.faceImage,
            fingerImage: this.props.route.params.fingerImage,
            cardFront: this.state.uri_front,
            cardBack: this.state.uri_back
        })
    }
    render() {
        return (
            <Layout style={styles.container}>
                <Layout style={{flex: 1, flexDirection:'column', alignItems:'center', overflow:'hidden'}}>
                    <Image source={this.state.uri_front ? {uri: this.state.uri_front} : require('../assets/placeholder.jpg')} style={{flex: 1, marginBottom: 16, width:"100%"}}/>
                    <Image source={this.state.uri_back ? {uri: this.state.uri_back} : require('../assets/placeholder.jpg')}  style={{flex: 1, marginBottom: 16, width:"100%"}}/>
                </Layout>
                <Layout style={{flexDirection: 'row', justifyContent:'center', marginBottom: 16}}>
                    <ButtonGroup  size='small'> 
                        <Button disabled={this.state.requesting} onPress={this.handleNavigateFront} accessoryLeft={style=><Icon {...style} name="person-outline"/>}>Capture front side</Button>
                        <Button disabled={this.state.requesting} onPress={this.handleNavigateBack}  accessoryLeft={style=><Icon {...style} name="credit-card-outline"/>}>Capture back side</Button>
                    </ButtonGroup>
                </Layout>
                <Button accessoryLeft={this.state.requesting ? LoadingIndicator : style=><Icon {...style} name="cloud-upload-outline"/>} disabled={this.state.requesting || !this.state.uri_back || !this.state.uri_front} onPress={this.getOCR}>Submit</Button>
            </Layout>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        padding: 16,
    },
    buttonGroup: {
      margin: 8
    },
    btn: {
        margin: 4
    }
});
export default OcrStep
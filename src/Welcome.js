import {
    Button,
    Icon,
    ButtonGroup,
    Spinner,
    Card,
    Modal,
    Layout,
    Input,
    ListItem,
    List,
  } from '@ui-kitten/components';
import { StyleSheet, View, Text, Image } from 'react-native';
import React from 'react';
import { request } from './api';
const LoadingIndicator = (props) => (
    <View style={[props.style, styles.indicator]}>
      <Spinner status="basic" size='small'/>
    </View>
);
const LoginTag = (props) => (
    <Button size='tiny' appearance='outline'>
      Logged in
    </Button>
  );
const RegisterTag = (props) => (
    <Button size='tiny' >
        Register
    </Button>
);
const add0 = (x) => {
    if (x < 10) return "0" + x
    return x
}
const formatTime = (d) => {
    let tmp = new Date(d * 1000)
    let day = tmp.getDay()
    let month = tmp.getMonth() + 1
    let year = tmp.getFullYear()
    let h = tmp.getHours()
    let minute = tmp.getMinutes()
    return `${add0(day)}/${add0(month)}/${year} ${add0(h)}:${add0(minute)}`
}
class Welcome extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showModal: false,
            requestInfo: false,
            name: "",
            address: "",
            birthday: "",
            id:"",
            history: []
        }
    }
    componentDidMount() {
        this.setState({requestInfo: true})
        const {aliasId} = this.props.route.params
        request().getInfo(aliasId)
        .then(res => {
            if (res.data.success) {
                const {name, address, birthday, id} = res.data.data
                this.setState({name, address, birthday, id, history: res.data.history.map((item,idx) => res.data.history[res.data.history.length-1-idx]), requestInfo: false})
            }
        })
    }
    render() {
        const {name, address, birthday, id, history} = this.state
        console.log(history)
        return (
            <Layout style={styles.container}>
                {
                    this.state.requestInfo ? <Spinner /> 
                    :
                    <>
                        <Modal
                            visible={this.state.showModal}
                            backdropStyle={styles.backdrop}
                            onBackdropPress={() => this.setState({showModal: false})}>
                            <Card disabled={true}>
                                <Input label="Name" value={name} disabled={true} />  
                                <Input label="ID Number" value={id} disabled={true} />  
                                <Input label="Birthday" value={birthday} disabled={true} /> 
                                <Input label="Address" value={address} disabled={true} /> 
                            </Card>
                        </Modal>
                        <Text style={{color: "#2194EA", fontSize: 32}}>Hi, {name}</Text>
                        <List
                            style={styles.listContainer}
                            data={history}
                            renderItem={({item, index})=><ListItem title={`${formatTime(item)}`} accessoryRight={(index == this.state.history.length - 1) ? RegisterTag : LoginTag}/>}
                        />
                        <ButtonGroup>
                            <Button onPress={()=>this.setState({showModal: true})} accessoryLeft={x => <Icon name="person-outline" {...x} />}>Account</Button>
                            <Button onPress={()=>this.props.navigation.navigate("FaceRecord")} accessoryLeft={x => <Icon name="log-out-outline" {...x} />}>Logout</Button>
                        </ButtonGroup>
                    </>
                }
                
                
            </Layout>
        )
    }
    
}
const styles = StyleSheet.create({
    listContainer: {
        flex: 1,
        width:"100%",
        marginBottom: 16
      },
    container: {
        flex: 1,
        alignItems: "center",
        flexDirection: 'column',
        padding: 16,
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
});
export default Welcome
# install

```sh
$ npm install
```

# API KEY

Create apiKeyEnv.js in the root folder.

Edit and put the api keys here:

```
module.exports = {
    FACEID_RECOGNIZE: "API_KEY_FOR_FACEID_RECOGNIZE",
    FACEID_REGISTER: "API_KEY_FOR_FACEID_REGISTER",
    OCR_FRONTBACK: "API_KEY_FOR_OCR",
    EKYC_MATCH: "API_KEY_FOR_eKYC_MATCH",
    HEADPOSE: "API_KEY_FOR_HEADPOSE",
}
```

# Debug

```sh
$ npm run android
```

# Build apk

```sh
$ cd android
$ ./gradlew assembleRelease
$ adb install -r ./app/build/outputs/apk/release/app-release.apk
```
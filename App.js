import React from 'react';
import { TopNavigation, ApplicationProvider, Layout, IconRegistry, Text } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import 'react-native-gesture-handler';

import { View, StyleSheet } from 'react-native';
import * as eva from '@eva-design/eva';
import { default as appTheme } from './custom-theme.json';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import OcrStep from './src/OcrStep'
import OcrStepResult from './src/OcrStepResult'
import Camera from './src/Component/Camera'
import EditPhoto from './src/Screen/EditPhoto';
import FaceRecord from './src/FaceRecord'
import Success from './src/Success'
import CameraHeadPose from './src/Component/CameraHeadPose'
import { default as mapping } from './mapping.json'; 
import Welcome from './src/Welcome'
import FingerStep from './src/FingerStep';
import CameraFinger from './src/Component/CameraFinger'
import EditPhotoFinger from './src/Screen/EditPhotoFinger'
const Stack = createStackNavigator();

const theme = { ...eva.light, ...appTheme };
const stepName = ["1. OCR", "Headpose", "Match"]
function HomeScreen() {
  console.log('render home')
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'black' }}>
      <Text>Home Screen</Text>
    </View>
  );
}
function NavStack() {
  return (
     <Stack.Navigator
        screenOptions={{
          headerShown: false
        }}
      >
      <Stack.Screen name="FaceRecord" component={FaceRecord} />
      <Stack.Screen name="FingerStep" component={FingerStep} />
      <Stack.Screen name="HeadposeCamera" component={CameraHeadPose} />
      <Stack.Screen name="Welcome" component={Welcome} />
      <Stack.Screen name="Camera" component={Camera} /> 
      <Stack.Screen name="CameraFinger" component={CameraFinger} /> 
      
      <Stack.Screen name="OcrStep" component={OcrStep} />
      <Stack.Screen name="Success" component={Success} />
      <Stack.Screen name="OcrStepResult" component={OcrStepResult} />
      <Stack.Screen name="EditPhoto" component={EditPhoto} />
      <Stack.Screen name="EditPhotoFinger" component={EditPhotoFinger} />
    </Stack.Navigator>
  );
}
export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentStep: 0
    }
  }
  render() {
    return (
      <React.Fragment>
        <IconRegistry icons={EvaIconsPack} />
        <ApplicationProvider {...eva} theme={theme} customMapping={mapping}>
            <SafeAreaProvider>
              <NavigationContainer>
                <NavStack initialRouteName={"Welcome"} />
              </NavigationContainer>
            </SafeAreaProvider>
        </ApplicationProvider>
      </React.Fragment>
  )}
}

const styles = StyleSheet.create({
  titleStep: {
    borderRadius: 4,
    padding: 8,
    margin: 4,
    width: '100%',
    textAlign: 'left',
    backgroundColor: '#2194EA',
  },
  container: {
    padding: 16,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
})